<?php

trait Sharable{
    public function share(){
        return "the name of the trait is :".__TRAIT__;
    }
}

class Post{
    use Sharable;
}
$post= new Post;
echo $post->share();
?>

